# Contributions
All contributions to this repo must be made on either a fork or another branch (for those with repo access).

## Merge Requests
For each merge request, please include a small description of what the proposed changes are. If you're adding a new feature that is listed in the issue tracker, a title of _fixes #x_ is preferred, since this will automatically close the issue when merged.

## Access
All changes to the `master` branch must be approved by DerpyChap beforehand.  

## Changelog
The changelog file should follow the following template:

```markdown
# Changelog
Changelog file description.

## 10th January 2069

### Added
- Feature 1
- Feature 2
- Feature 3

### Changed
- Change 1
- Change 2
- Change 3

## 5th January 2069

### Changed
- Change 1
...
```

The `##` header should be the current date of the changelog update. The changelog is split into types of changes, you should use any of the following types:

- `Added` for new features
- `Changed` for changes in existing functionality
- `Deprecated` for soon-to-be removed features
- `Removed` for now removed features
- `Fixed` for any bug fixes
- `Security` in case of vulnerabilities.

If multiple branches are planned to be merged, then only one branch should update the changelog including changes from all branches being merged. This is to avoid any spam with the bot. If there are multiple changes to the changelog in one day, then the time should also be included in the changelog like so: `10th January 2069, 4:20PM BST`. Any new updates to the changelog must be separate to the others, even if they're on the same day.
