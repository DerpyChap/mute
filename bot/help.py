import discord
import asyncio
import inspect
import textwrap
import os
from discord.ext import commands
from cogs.bot.utils import help as h
from cogs.utils import checks


class help(commands.Cog):
    """Custom help messages WITH FANCY EMBEDS OOOOOO!"""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'General',
            'image': 'https://i.imgur.com/AZWeMcH.png'
        }

    def field_formatter(self, string: str):
        """Formats an embed field so it's smol."""
        return textwrap.fill(string, 25)

    async def help_groups(self):
        """Returns all different help groups and their commands."""
        groups = {'General': []}
        for command in self.liara.commands:
            cog = command.cog_name
            if cog == 'Core':
                if not groups.get('Core'):
                    groups['Core'] = [command]
                else:
                    groups['Core'].append(command)
            elif cog:
                cog = self.liara.get_cog(cog)
                try:
                    settings = cog.help_set
                    if settings.get('group'):
                        if not groups.get(settings['group']):
                            groups[settings['group']] = [command]
                        else:
                            groups[settings['group']].append(command)
                    else:
                        groups['General'].append(command)
                except AttributeError:
                    groups['General'].append(command)
            else:
                groups['General'].append(command)
        return groups

    async def can_run(self, ctx, command):
        for function in command.checks:
            try:
                if inspect.iscoroutinefunction(function):
                    if not await function(ctx):
                        return False
                else:
                    if not function(ctx):
                        return False
            except commands.NoPrivateMessage:
                pass
            except:
                return False
        return True

    async def help_formatter(self, ctx):
        groups = await self.help_groups()
        embeds = []
        # Building the top embed
        emb = discord.Embed(
            description=
            "M.U.T.E (A.K.A The Mega Underwhelming Text Experience) enables you to have text channels for any of your active voice channels. "
            "Developed by DerpyChap.")
        prefix = self.liara.command_prefix[0]
        emb.add_field(
            name='Made possible using',
            value=
            '🐳 [Liara Docker](https://gitlab.com/nerd3-servers/liara-docker)')
        emb.add_field(name="Source",
                      value='💾 [Gitlab](https://gitlab.com/DerpyChap/mute)')
        emb.set_author(name='❓ Bot Help')
        emb.set_thumbnail(url=ctx.me.avatar_url_as(format='png'))

        for group in groups:
            if group == 'Core' and not checks.owner_check(ctx):
                continue
            cmd_group = sorted(groups[group], key=lambda x: x.name)
            l = []
            for command in cmd_group:
                hidden = False
                if command.hidden:
                    hidden = True
                    if not checks.owner_check(ctx):
                        # Not a bot owner, so don't show them the hidden command
                        continue
                info = command.short_doc if command.short_doc else "No description provided."
                l.append(' - `{}{}`{}\n{}'.format(prefix, command.name,
                                                  '🕵' if hidden else '', info))
            emb.add_field(name='{} Commands'.format(group), value='\n'.join(l))

        emb.set_footer(text="Do {}help <command> for more info on a command.".
                       format(prefix))

        # Let's give the embeds some colour
        if not isinstance(ctx.channel, discord.abc.PrivateChannel):
            for emb in embeds:
                emb.colour = ctx.guild.me.colour

        # Returns the embed
        return emb

    @commands.command()
    async def help(self, ctx, *, command=None):
        """Returns a list of all commands you can run, or help on a specific command.
        
        Example
        -------
        `[p]help setup`"""
        if not command:
            try:
                await ctx.message.delete()
            except:
                pass
            emb = await self.help_formatter(ctx)
            try:
                await ctx.author.send(embed=emb)
            except:
                await ctx.send(embed=emb)
                return
        else:
            command = self.liara.get_command(command)
            if isinstance(ctx.channel, discord.abc.PrivateChannel):
                if await self.can_run(ctx, command):
                    emb = await h.commandEmbed(ctx, command)
                    await ctx.send(embed=emb)
            else:
                if (command and await command.can_run(ctx)):
                    emb = await h.commandEmbed(ctx, command)
                    colour = ctx.guild.me.colour
                    emb.colour = colour
                    await ctx.send(embed=emb)


def setup(liara):
    liara.remove_command('help')
    liara.add_cog(help(liara))
