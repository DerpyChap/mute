import discord


async def commandEmbed(ctx, command: discord.ext.commands.command = None):
    """Returns an embed with the command help info."""
    if not command:
        command = ctx.command
    liara = ctx.bot
    name = command.qualified_name
    cog = command.cog_name
    desc = command.help
    prefix = liara.command_prefix[0]
    title = '**{} - {}**'.format(cog.capitalize() if cog.islower() else cog,
                                 name) if cog else name
    emb = discord.Embed(description=title)
    emb.set_author(name='❓ Command Help')
    params = []
    for param in command.clean_params:
        params.append('<{}>'.format(param))
    if params:
        params = ' '.join(x for x in params)
        value = '`{}{} {}`'.format(prefix, command.qualified_name, params)
    else:
        value = '`{}{}`'.format(prefix, command.qualified_name)
    emb.add_field(name='Usage', value=value)
    #emb.add_field(name='Description', value=desc, inline=False)
    if desc:
        desc_list = desc.split('-------')
        list_final = []
        for i, item in enumerate(desc_list):
            if i == 0:
                info = {'title': 'Description', 'contents': item}
                list_final.append(info)
            else:
                prev = i - 1
                p = list_final[prev]
                title = p['contents'].splitlines()[-1]
                p['contents'] = p['contents'][:p['contents'].
                                              rfind('\n{}\n'.format(title))]
                list_final[prev] = p
                info = {'title': title, 'contents': item}
                list_final.append(info)

        for item in list_final:
            emb.add_field(name=item['title'],
                          value=item['contents'].replace('[p]', prefix),
                          inline=False)

    try:
        subcommands = command.commands
        if subcommands:
            subString = []
            for cmd in subcommands:
                if await cmd.can_run(ctx):
                    string = '**{}** - {}'.format(cmd.name, cmd.short_doc)
                    subString.append(string)
            if subString:
                subString = '\n'.join(x for x in subString)
                emb.add_field(name='Subcommands', value=subString)
                emb.set_footer(
                    text="To use a subcommand, do {}{} <subcommand>".format(
                        prefix, name))
    except AttributeError:
        pass

    if cog:
        try:
            settings = liara.get_cog(cog).help_set
            if settings.get('image'):
                emb.set_thumbnail(url=settings['image'])
        except AttributeError:
            pass
    return emb


async def ctxEmbed(ctx):
    """Returns a command embed from a context object."""
    emb = await commandEmbed(ctx)
    if not isinstance(ctx.channel, discord.abc.PrivateChannel):
        colour = ctx.guild.me.colour
        emb.colour = colour
    return emb


async def sendHelp(ctx):
    """Sends help to the context object."""
    await ctx.send(embed=await ctxEmbed(ctx))
