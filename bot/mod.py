import discord
import asyncio
import os
import random
from discord.ext import commands
from cogs.utils import checks
from cogs.utils.storage import RedisCollection
from cogs.bot.utils import help, config

emotes = config.Load('emotes')
loading = emotes.get('loading')


class Mod(commands.Cog):
    """Voice related mod commands."""

    def __init__(self, liara):
        self.liara = liara
        self.db = RedisCollection(liara.redis, 'bot.voicetext')
        self.help_set = {
            'group': 'Mod',
            'image': 'https://i.imgur.com/TNfz8TX.png'
        }
        self.settings = {
            'category': None,
            'channels': {},
            'video': True,
            'block_video': False
        }

    async def hirearchy_check(self, user: discord.Member,
                              moderator: discord.Member):
        """Does checks to make sure a mod can take actions against a user."""
        userrole = user.roles[-1]
        modrole = moderator.roles[-1]
        bot = user.guild.me
        if bot == user:
            return "That ain't right."
        if user == moderator:
            return "Self harm is bad, m'kay?"
        if user == user.guild.owner:
            return 'They own the server, you spoon.'
        if moderator != user.guild.owner:  #Otherwise an owner without a role would not be able to do stuff
            if userrole >= modrole:
                return 'Can\'t kick people above you, my dude.'

    async def voicekick(self,
                        user: discord.Member,
                        mod: discord.Member,
                        reason: str = None):
        """Removes a user from a voice channel."""
        guild = user.guild
        bot = guild.me
        hcheck = await self.hirearchy_check(user, mod)
        if hcheck:
            raise ValueError(hcheck)
        if not (user.voice is not None and user.voice.channel is not None):
            raise ValueError('That user is not in a voice channel.')
        before = user.voice.channel
        p = before.permissions_for(bot)
        if not p.move_members:
            raise ValueError(
                f'I cannot remove users from **{before.name}**. Check my permissions for that channel and try again.'
            )
        audit_reason = f'{str(mod)} voicekicked {str(user)} from {before.name}'
        if reason:
            audit_reason += f' with reason: {reason}'
        await user.move_to(None, reason=audit_reason)
        return before

    async def massmove(self, from_channel: discord.VoiceChannel,
                       to: discord.VoiceChannel):
        """Mass moves all users from one channel to the other channel."""
        guild = from_channel.guild
        bot = guild.me

        p_from = from_channel.permissions_for(bot)
        p_to = to.permissions_for(bot)
        if not p_from.move_members:
            raise ValueError(
                f'I cannot move users from **{from_channel.name}** . Check my permissions for that channel and try again.'
            )
        if not p_to.read_messages:
            raise ValueError(
                f'I cannot move users to **{to.name}** . Check my permissions for that channel and try again.'
            )

        members = from_channel.members
        count = 0
        vt = self.liara.get_cog('voicetext')
        if vt:
            channel, vcSet = await vt.fetchChannel(to)
            if vcSet and not channel:
                channel, vcSet = await vt.createChannel(to)
        for m in members:
            if m.voice.channel == from_channel:
                try:
                    await m.move_to(to, reason='Mass moving...')
                    count += 1
                except:
                    pass
        return count

    @commands.command(name='voicekick', aliases=['disconnect'], hidden=True)
    @checks.mod_or_permissions(move_members=True)
    async def _voicekick(self, ctx, user: discord.Member, *,
                         reason: str = None):
        """Removes the specified user from any voice channel they're in.

        Do note you can't voicekick people equal to or above you in the hirearchy."""
        try:
            c = await self.voicekick(user, ctx.author, reason)
        except discord.Forbidden:
            return await ctx.send(
                'Looks like my permissions don\'t allow me to do that.')
        except ValueError as e:
            return await ctx.send(e)
        except:
            return await ctx.send(
                'An unexpected error occured when trying to do that.')
        await ctx.send(f'Kicked {user.mention} from **{c.name}**.')

    @commands.command(name='massmove', aliases=['mmove'])
    @checks.mod_or_permissions(move_members=True)
    async def _massmove(self,
                        ctx,
                        from_channel: discord.VoiceChannel,
                        to: discord.VoiceChannel = None):
        """Moves all users from one Voice Channel to another.

        If you are in a Voice Channel, just specifying a target channel will move all users in your channel to the target.
        
        The quickest way of specifying channels would be to use Developer Mode. To enable Developer Mode, go to User Settings > Appearance and check Developer Mode.
        After that, you can right click the Voice Channel(s) you want, select Copy ID and paste them in place of `from` and `to`.
        
        Examples
        -------
         - `[p]massmove "Channel A" "Channel B"`
         - `[p]massmove "Channel B"` (Mass moves users in your channel to Channel B)
         - `[p]massmove 510148276516028469 510155647145213954` (Using Channel IDs in place of names)"""
        if not to:
            vc = ctx.author.voice.channel
            if not vc:
                return await ctx.send(
                    'You are not in a voice channel. Please specify a channel to move from.',
                    embed=await help.ctxEmbed(ctx))
            else:
                to = from_channel
                from_channel = vc
        p_from = from_channel.permissions_for(ctx.author)
        p_to = to.permissions_for(ctx.author)
        if not p_from.move_members:
            return await ctx.send(
                f'You cannot move users from **{from_channel.name}**.')
        if not p_to.read_messages:
            return await ctx.send(f'You cannot move users to **{to.name}**.')
        count = len(from_channel.members)
        if count == 0:
            return await ctx.send(f'There are no users in **{from_channel}**.')
        msg = await ctx.send(
            f'{loading} Moving **{count}** user{"s" if count != 1 else ""} from **{from_channel.name}** to **{to.name}**...'
        )
        try:
            n = await self.massmove(from_channel, to)
        except discord.Forbidden:
            return await msg.edit(
                content='Looks like my permissions don\'t allow me to do that.')
        except ValueError as e:
            return await msg.edit(content=e)
        except:
            return await msg.edit(
                content='An unexpected error occured when trying to do that.')
        await msg.edit(
            content=
            f'Moved **{n}** user{"s" if n != 1 else ""} from **{from_channel.name}** to **{to.name}**.'
        )

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def screenshare(self, ctx):
        """Toggle whether or not people are allowed to screenshare on the server.
        
        This is not the same as `[p]link`, which just provides a link to the voice channel in the description."""
        settings = await self.db.get(ctx.guild.id, self.settings)
        prefix = self.liara.command_prefix[0]
        s = settings.get('block_video', False)
        if not s:
            settings['block_video'] = True
            msg = ("Screenshare attempts will now be blocked. "
                   "Do note that roles above my own will not be "
                   "blocked from screensharing.")
            if settings.get('video', False):
                msg += ("\n\nYou may also want to hide the Voice "
                        "Channel URL from the voice text channel "
                        f"description via `{prefix}link`.")
        else:
            settings['block_video'] = False
            msg = "Screenshare attempts will no longer be blocked. "
            if not settings.get('video', False):
                msg += ("If screenshare is unavailable on your server, use "
                        f"`{prefix}link` to add a Voice Channel link "
                        "to any voice-text channel description.")
            else:
                msg += ("You can use the URL provided in voice-text channels "
                        "to screenshare if it's unavailable on your server. ")
            msg += (
                "\n\nNote that this behaviour is not offically supported or "
                "endorsed by Discord or M.U.T.E.")
        await self.db.set(ctx.guild.id, settings)
        await ctx.send(msg)

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        guild = member.guild
        bot = guild.me

        settings = await self.db.get(guild.id, self.settings)
        if not settings.get('block_video', False):
            return

        if await self.hirearchy_check(member, bot):
            return
        if after.self_video and not before.self_video:
            reason = 'Attempting to screenshare'
            try:
                await member.send(
                    f'Sorry, screensharing is not allowed on {guild.name}.')
            except:
                reason += ', notification DM failed to send'
            try:
                await self.voicekick(member, bot, reason)
            except discord.Forbidden:
                return
            except discord.HTTPException:
                return
            except ValueError:
                return
            except Exception as e:
                print(
                    f'Something went wrong while trying to kick a user from voice: {e}'
                )
                return


def setup(liara):
    cog = Mod(liara)
    liara.add_cog(cog)
