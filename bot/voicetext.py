import aiohttp
import discord
import asyncio
from discord.ext import commands
from cogs.utils import checks
from cogs.utils.storage import RedisCollection
from cogs.bot.utils import config
from typing import Union
from datetime import datetime, timedelta

emotes = config.Load('emotes')
mobile = emotes.get('mobile')
owner = emotes.get('owner')
bot = emotes.get('bot')


class voicetext(commands.Cog):
    """DerpyChap does what Discan't"""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'Admin',
            'image': 'https://i.imgur.com/RDTpSCa.png'
        }
        self.db = RedisCollection(self.liara.redis, 'bot.voicetext')
        self.settings = {
            'category': None,
            'channels': {},
            'video': True,
            'block_video': False,
            'been_mentioned': False
        }
        self.delete_queue = []

    async def permsCheck(self, channel: discord.CategoryChannel):
        bot = channel.guild.me
        p = channel.permissions_for(bot)
        failed = []
        if not p.manage_channels:
            failed.append('Manage Channel')
        if not p.manage_roles:
            failed.append('Manage Permissions')
        if not p.read_messages:
            failed.append('Read Text Channels & See Voice Channels')
        if not p.send_messages:
            failed.append('Send Messages')
        return failed

    def icons(self, user):
        icons = ''
        if user.bot:
            icons += bot
        if user == user.guild.owner:
            icons += owner
        if user.is_on_mobile():
            icons += mobile
        return icons

    async def mention(self, user):
        settings = await self.db.get(user.guild.id, self.settings)
        mentioned = settings.get('mentioned', [])
        if user.id not in mentioned:
            mentioned.append(user.id)
            settings['mentioned'] = mentioned
            await self.db.set(user.guild.id, settings)
            return user.mention
        else:
            return str(f'**{user.name}**')

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def setup(self, ctx, *, name: str = "Voice Text"):
        """Automatically creates a category hidden from `@everyone` with the relevant permissions.
        
         - `name` - The name of the category you'd like to make.

        If no `name` is specified, then the Category name will be "Voice Text".
        
        If you would like to create a category manually, use the `<voicechat` command instead."""
        settings = await self.db.get(ctx.guild.id, self.settings)
        overwrites = {
            ctx.guild.default_role:
            discord.PermissionOverwrite(read_messages=False),
            ctx.guild.me:
            discord.PermissionOverwrite(read_messages=True,
                                        send_messages=True,
                                        manage_channels=True,
                                        manage_roles=True,
                                        move_members=True)
        }
        try:
            channel = await ctx.guild.create_category_channel(
                name=name,
                overwrites=overwrites,
                reason='Creating Voice Text category by request of {}'.format(
                    str(ctx.author)))
        except discord.Forbidden:
            return await ctx.send(
                'I\'m lacking some permissions to do this, please look here for a list of permissions I use: https://gitlab.com/DerpyChap/mute/'
            )
        except Exception as e:
            return await ctx.send(
                'An unexpected error occured while trying to make the category. Try again in a bit.'
            )
        settings['category'] = channel.id
        settings['mentioned'] = []
        await self.db.set(ctx.guild.id, settings)
        await ctx.send(
            "Okay, any voice-text channels will be created under the {} category.\n\n"
            "Please do not change my permissions for this category as I need them to work correctly.\n\n"
            "You can, however, rename the category, change its position and adjust the permissions for any other role or user to your liking.\n\n"
            "To disable the creation of Voice Text channels, use the `<voicechat` command with no arguments."
            .format(name))

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def voicechat(self, ctx, *, category: discord.CategoryChannel = None):
        """Set the category M.U.T.E. to use for its voice-text channels.

        I recommend removing `@everyone`'s Read Messages permission from that category so that only voice participants can see their channel.
        
        Leave blank to disable the creation of voice-text channels."""
        settings = await self.db.get(ctx.guild.id, self.settings)
        if category:
            f = await self.permsCheck(category)
            if f:
                l = '\n'.join([' - {}'.format(p) for p in f])
                return await ctx.send(
                    'I do not have the following permissions for that Category:\n\n{}'
                    .format(l))
            settings['category'] = category.id
            await ctx.send(
                'I will now create any voice-text channels in {}.'.format(
                    category.name))
        else:
            settings['category'] = None
            await ctx.send(
                'I will no longer create any text channels for voice participants.'
            )
        settings['mentioned'] = []
        await self.db.set(ctx.guild.id, settings)

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def link(self, ctx):
        """Toggle the visibility of the voice channel URL in the text channel description.
        
        To stop people from screensharing completely, use `[p]screenshare`"""
        settings = await self.db.get(ctx.guild.id, self.settings)
        prefix = self.liara.command_prefix[0]
        toggle = settings.get('video', True)
        if toggle:
            settings['video'] = False
            msg = 'OK, I\'ll no longer display a link to the voice channel in the description.'
            if not settings.get('block_video', False):
                msg += f'\n\nIf you want to block people from being able to screenshare completely, use `{prefix}screenshare`.'
        else:
            settings['video'] = True
            msg = 'OK, I\'ll now display a link to the voice channel in the description.'
            if settings.get('block_video', False):
                msg += f'\n\nNote: Screenshare blocking is enabled, to disable it use `{prefix}screenshare`.'
        await self.db.set(ctx.guild.id, settings)
        await ctx.send(msg)

    async def createChannel(self, vc: discord.VoiceChannel):
        """Creates a dedicated text channel for a voice channel.

        Note that this will override any existing text channel assigned to this VC.
        
        Returns None if the category doesn't exist."""
        guild = vc.guild
        if guild.afk_channel == vc:
            return (None, None)
        settings = await self.db.get(guild.id, self.settings)
        if not settings['category']:
            return (None, None)
        category = guild.get_channel(settings['category'])
        if not category:
            return (None, None)
        name = vc.name[0:95] + '-text'
        tc = await guild.create_text_channel(
            name,
            category=category,
            reason='Creating text channel for {}.'.format(vc.name))
        vcSet = {'channel': tc.id, 'users': []}
        settings['channels'][vc.id] = vcSet
        await self.db.set(guild.id, settings)
        topic = f'\🔊 **{vc.name}**'
        if settings.get('video', True):
            topic += ' | https://discordapp.com/channels/{0.guild.id}/{0.id}'.format(
                vc)
        await tc.edit(topic=topic)
        return (tc, vcSet)

    async def fetchChannel(self, vc: discord.VoiceChannel):
        """Fetches a voice channel's related text channel. Returns a Tuple of the text channel object and the VC's database entry.
        
        Returns None if the channel doesn't exist or the guild has no category defined."""
        guild = vc.guild
        settings = await self.db.get(guild.id, self.settings)
        if not settings['category']:
            return (None, None)
        vcSet = settings['channels'].get(vc.id, {'channel': None, 'users': []})
        if not vcSet['channel']:
            return (None, vcSet)
        tc = guild.get_channel(vcSet['channel'])
        return (tc, vcSet)

    async def deleteChannel(self,
                            vc: Union[discord.VoiceChannel, int],
                            guild: discord.Guild = None):
        """Deletes a voice channel's text channel. Also removes the Voice Channel from the database."""
        if isinstance(vc, int):
            if not guild:
                return
            chanId = vc
        else:
            chanId = vc.id
            guild = vc.guild
        settings = await self.db.get(guild.id, self.settings)
        if not settings['category']:
            return
        vcSet = settings['channels'].get(chanId, {'channel': None, 'users': []})
        if not vcSet['channel']:
            return
        settings['channels'].pop(chanId)
        await self.db.set(guild.id, settings)
        try:
            tc = guild.get_channel(vcSet['channel'])
            await tc.delete()
        except AttributeError:
            pass
        return

    async def queueDelete(self, vc: discord.VoiceChannel):
        """Adds a Voice Text channel to the deletion queue."""
        for d in list(self.delete_queue):
            if d['vc'] == vc:
                self.delete_queue.remove(d)
        self.delete_queue.append({'vc': vc, 'date': datetime.utcnow()})

    async def autoDelete(self):
        while not self.liara.is_ready():
            await asyncio.sleep(0.1)
        while self is self.liara.get_cog('voicetext'):
            for d in list(self.delete_queue):
                diff = datetime.utcnow() - d['date']
                if diff >= timedelta(seconds=10):
                    self.delete_queue.remove(d)
                    await self.deleteChannel(d['vc'])
            await asyncio.sleep(0.01)

    async def addUser(self, vc: discord.VoiceChannel, user: discord.Member):
        """Adds a user to a voice channel's text channel."""
        guild = vc.guild
        for d in list(self.delete_queue):
            if d['vc'] == vc:
                self.delete_queue.remove(d)
        channel, vcSet = await self.fetchChannel(vc)
        if not vcSet:
            return
        if not channel:
            channel, vcSet = await self.createChannel(vc)
            if not channel:
                return
        users = vcSet.get('users', [])
        if user.id not in users:
            users.append(user.id)
            vcSet['users'] = users
            settings = await self.db.get(guild.id, self.settings)
            settings['channels'][vc.id] = vcSet
            await self.db.set(guild.id, settings)
        overwrite = discord.PermissionOverwrite()
        overwrite.send_messages = True
        overwrite.read_messages = True
        try:
            await channel.set_permissions(user,
                                          overwrite=overwrite,
                                          reason='User joined {}.'.format(
                                              vc.name))
            await channel.send('{}{} joined your channel.'.format(
                await self.mention(user), self.icons(user)))
        except discord.NotFound:
            return
        return

    async def removeUser(self, vc: discord.VoiceChannel,
                         user: Union[discord.Member, int]):
        """Removes a user from a voice channel's text channel. If user is a user ID, then it will be removed from the database.
        
        Will also delete the text channel if it's empty."""
        if isinstance(user, int):
            userId = user
        else:
            userId = user.id
        guild = vc.guild
        channel, vcSet = await self.fetchChannel(vc)
        if not channel:
            return
        if isinstance(user, discord.Member):
            await channel.set_permissions(user,
                                          overwrite=None,
                                          reason='User left {}.'.format(
                                              vc.name))
        users = vcSet.get('users', [])
        if userId in users:
            users.remove(userId)
            vcSet['users'] = users
            settings = await self.db.get(guild.id, self.settings)
            settings['channels'][vc.id] = vcSet
            await self.db.set(guild.id, settings)
        if isinstance(user, discord.Member) and users:
            await channel.send('{}{} left your channel.'.format(
                await self.mention(user), self.icons(user)))
        if not users:
            await channel.send(
                '⚠ All users have left. Channel will be removed if empty for longer than 10 seconds.'
            )
            await self.queueDelete(vc)
        return

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        guild = member.guild
        settings = await self.db.get(guild.id, self.settings)
        if not settings['category']:
            return
        if before.channel != after.channel:
            if after.channel:
                if not after.channel.permissions_for(guild.me).read_messages:
                    return
                if guild.afk_channel == after.channel:
                    if not before.channel:
                        return
                    else:
                        await self.removeUser(before.channel, member)
                        return
                await self.addUser(after.channel, member)
            if before.channel:
                await self.removeUser(before.channel, member)

    @commands.Cog.listener()
    async def on_guild_channel_update(self, before, after):
        guild = before.guild
        if isinstance(before, discord.VoiceChannel):
            if before.permissions_for(
                    guild.me).read_messages and not after.permissions_for(
                        guild.me).read_messages:
                await self.deleteChannel(after)
            elif after.permissions_for(
                    guild.me).read_messages and not before.permissions_for(
                        guild.me).read_messages:
                c = await self.fetchChannel(after)
                if not c[0] and after.members:
                    await self.createChannel(after)
                    for m in after.members:
                        await self.addUser(after, m)
            if before.name != after.name:
                c = await self.fetchChannel(after)
                if c[0]:
                    settings = await self.db.get(guild.id, self.settings)
                    topic = f'\🔊 **{after.name}**'
                    if settings.get('video', True):
                        topic += ' | https://discordapp.com/channels/{0.guild.id}/{0.id}'.format(
                            after)
                    await c[0].edit(name=after.name[0:95] + '-text',
                                    topic=topic)
        if isinstance(before, discord.CategoryChannel):
            settings = await self.db.get(guild.id, self.settings)
            if settings['category'] == before.id:
                if before.position != after.position:
                    settings['mentioned'] = []
                    await self.db.set(guild.id, settings)

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        guild = channel.guild
        if isinstance(channel,
                      discord.VoiceChannel) and channel.permissions_for(
                          guild.me).read_messages:
            textChannel = await self.fetchChannel(channel)
            if not textChannel[0]:
                return
            await self.deleteChannel(channel)
        else:
            settings = await self.db.get(guild.id, self.settings)
            for v, s in settings['channels'].items():
                if s['channel'] == channel.id:
                    vc = guild.get_channel(v)
                    if vc:
                        if vc.members and vc.permissions_for(
                                guild.me
                        ).read_messages and vc != guild.afk_channel:
                            await self.createChannel(vc)
                            for m in vc.members:
                                await self.addUser(vc, m)

    async def cleanup(self, guild: discord.Guild):
        settings = await self.db.get(guild.id, self.settings)
        if not settings['category']:
            return
        category = guild.get_channel(settings['category'])
        if not category:
            return
        for vc in guild.voice_channels:
            if not vc.permissions_for(guild.me).read_messages:
                continue
            if guild.afk_channel == vc:
                continue
            vcSet = settings['channels'].get(vc.id, {
                'channel': None,
                'users': []
            })
            toBeAdded = []
            members = vc.members
            channel = await self.fetchChannel(vc)
            toBeRemoved = []
            for member in members:
                if member.id not in vcSet['users']:
                    toBeAdded.append(member)
            for uid in vcSet['users']:
                member = guild.get_member(uid)
                if not member:
                    toBeRemoved.append(uid)
                if member not in members:
                    toBeRemoved.append(member)
            if not channel[0] and (toBeAdded or toBeRemoved):
                await self.createChannel(vc)
            for member in toBeAdded:
                await self.addUser(vc, member)
            for member in toBeRemoved:
                await self.removeUser(vc, member)
            if channel and not members:
                await self.deleteChannel(vc)
        for vc in settings['channels']:
            channel = guild.get_channel(vc)
            if not channel:
                await self.deleteChannel(vc, guild)
            if not channel.permissions_for(guild.me).read_messages:
                await self.deleteChannel(channel, guild)

    @commands.Cog.listener()
    async def on_guild_available(self, guild):
        await self.cleanup(guild)

    @commands.Cog.listener()
    async def on_ready(self):
        for guild in self.liara.guilds:
            await self.cleanup(guild)

    async def cleanupLoop(self):
        await asyncio.sleep(120)
        while True:
            if self.liara.is_ready():
                for guild in self.liara.guilds:
                    await self.cleanup(guild)
            await asyncio.sleep(60)


def setup(liara):
    cog = voicetext(liara)
    liara.loop.create_task(cog.autoDelete())
    liara.loop.create_task(cog.cleanupLoop())
    liara.add_cog(cog)
