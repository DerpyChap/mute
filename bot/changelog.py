import discord
import asyncio
import os
import random
from discord.ext import commands
from cogs.utils import checks
from cogs.utils.storage import RedisCollection


class Changelog(commands.Cog):
    """Changes, of the log variety."""

    def __init__(self, liara):
        self.liara = liara
        self.db = RedisCollection(liara.redis, 'bot.changelog')
        self.help_set = {
            'group': 'General',
            'image': 'https://i.imgur.com/9pStvLY.png'
        }
        self.file = '/app/changelog/CHANGELOG.md'
        self.bot_name = os.environ['LIARA_NAME']
        self.lines = [
            'Beep boop, {} has received an update',
            'Watch out, {} has been updated', 'What\'s this? An update for {}?',
            'An update? Is {} hip and trendy like Discord yet?',
            '{} sucks less now!',
            '{}: Now with more features and less bugs than Discord itself!',
            '{} has been buffed',
            'Ch-ch-ch-ch-changes (Turn and face the strange)'
        ]

    @commands.command()
    @checks.admin_or_permissions(manage_server=True)
    async def changelog(self, ctx, channel: discord.TextChannel = None):
        """Set the channel for the changelog to appear in.
        
        Leave blank to disable."""
        servers = await self.db.get('servers', {})
        if channel:
            servers[ctx.guild.id] = channel.id
            await ctx.send(
                'I will now send any changelog updates to {}.'.format(
                    channel.mention))
        else:
            servers.pop(ctx.guild.id, None)
            await ctx.send(
                'I will no longer announce any changelog updates on this server.'
            )
        await self.db.set('servers', servers)

    @commands.command()
    async def changes(self, ctx):
        """View the bot's most recent changelog."""
        embed = await self.build_embed(
            title='Most recent changelog for {}'.format(self.bot_name))
        if not embed:
            return await ctx.send(
                'Hmmm, looks like my changelog is unavailable at the moment.')
        await ctx.send(embed=embed)

    async def build_embed(self, title: str = None):
        if not title:
            title = random.choice(self.lines).format(self.bot_name)
        type = ''
        changes = []
        f = open(self.file, 'r')
        version = ''
        lines = f.readlines()
        if lines:
            embed = discord.Embed(title=title, colour=0xE24329)
            for line in lines:
                if line.startswith('# '):
                    continue
                elif line.startswith('## '):
                    if version:
                        break
                    version = line[3:]
                    continue
                elif line.startswith('### '):
                    if not type:
                        type = line[4:]
                        continue
                    else:
                        if changes:
                            embed.add_field(name=type,
                                            value=''.join(changes),
                                            inline=False)
                        type = line[4:]
                        changes = []
                        continue
                if type:
                    changes.append(line)
            if version:
                embed.set_footer(
                    text=version,
                    icon_url=
                    'https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png'
                )
            if changes:
                embed.add_field(name=type, value=''.join(changes), inline=False)
            return embed

    async def announce(self):
        """Announces to all Voice Text channels."""
        vcdb = RedisCollection(self.liara.redis, 'bot.voicetext')
        for guild in self.liara.guilds:
            settings = await vcdb.get(guild.id, {
                'category': None,
                'channels': {},
                'video': True
            })
            if not settings['category']:
                continue
            category = guild.get_channel(settings['category'])
            if not category:
                continue
            for vc in guild.voice_channels:
                if not vc.permissions_for(guild.me).read_messages:
                    continue
                if guild.afk_channel == vc:
                    return
                vcSet = settings['channels'].get(vc.id, {
                    'channel': None,
                    'users': []
                })
                if vcSet['channel']:
                    channel = guild.get_channel(vcSet['channel'])
                    if channel:
                        await channel.send(
                            f'💾 **{self.bot_name}** has been updated! To view the changelog, run `{self.liara.command_prefix[0]}changes`.'
                        )

    async def check_loop(self):
        while not self.liara.is_ready():
            await asyncio.sleep(0.1)
        last = await self.db.get('last_modified', 0)
        await asyncio.sleep(3)
        while self is self.liara.get_cog('Changelog'):
            stamp = os.stat(self.file).st_mtime
            if stamp != last:
                embed = await self.build_embed()
                if embed:
                    await self.announce()
                    servers = await self.db.get('servers', {})
                    for server, channel in servers.items():
                        channel = self.liara.get_channel(channel)
                        if channel:
                            try:
                                await channel.send(embed=embed)
                            except:
                                pass
                last = stamp
                await self.db.set('last_modified', last)
            await asyncio.sleep(5)


def setup(liara):
    cog = Changelog(liara)
    liara.loop.create_task(cog.check_loop())
    liara.add_cog(cog)
