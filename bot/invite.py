import os
import discord
from discord.ext import commands


class invite(commands.Cog):
    """Posts the bot's invite link."""

    def __init__(self, liara):
        self.perms = os.environ['PERMISSIONS_INTEGER']
        self.int = '&permissions={}'.format(self.perms) if self.perms else ''
        self.liara = liara
        self.help_set = {
            'group': 'General',
            'image': 'https://i.imgur.com/ZjiChoY.png'
        }
        self.invite_url = None

    @commands.command()
    async def invite(self, ctx):
        """Returns M.U.T.E.'s invite URL for you to add to your own server."""
        if not self.invite_url:
            self.invite_url = 'https://discordapp.com/oauth2/authorize?client_id={}&scope=bot{}'.format(
                self.liara.user.id, self.int)
        await ctx.send(
            'You can invite me using the following link:\n\n<{}>'.format(
                self.invite_url))


def setup(liara):
    liara.add_cog(invite(liara))
