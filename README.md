# M.U.T.E. for Discord
M.U.T.E. (which stands for Mega Underwhelming Text Experience) dynamically creates and deletes text channels for voice channel participants, and adds some more extra features to Discord's voice channels. Each Voice Channel gets its own Text Channel, and once the voice channel is empty the channel will be deleted to avoid clutter.

M.U.T.E. uses [Liara Docker](https://gitlab.com/nerd3-servers/liara-docker) by myself and Jack Baron, which is a set of Docker and Docker Compose rules and cogs for the [modular Liara Discord bot](https://github.com/Thessia/Liara).

To invite M.U.T.E. to your server, [click here](https://discordapp.com/oauth2/authorize?client_id=510146857931112480&scope=bot&permissions=268438544). For help on how to use M.U.T.E. read below or run `<help` in your server.

## Setup (Automatic)
To setup M.U.T.E. on your server automatically. Use the `<setup` command. This will create a category hidden from `@everyone` and setup M.U.T.E.'s permissions for said category.

You are free to rename and move the channel (Running `<setup` followed by your desired category name will also create the category with said name), and are allowed to tweak the permissions of roles and users as needed, but please don't adjust M.U.T.E.'s permissions or you'll break things.

Once this category is made, you can revoke M.U.T.E.'s global permissions if you wish. Keep in mind it will need them if you want to remake the category.

## Setup (Manual)
To setup M.U.T.E. on your server manually, create a new Category (ideally private), and then run `<voicechat category`, where `category` is either the Category name or ID.

To make the most out of M.U.T.E., I strongly recommend removing `@everyone`'s `Read Messages` permission. That way only voice participants will be able to see their respective text channel. This is automatically done by Discord if you check `Private Category` and then check M.U.T.E.'s role when creating the Category.

If you want other roles to be able to view M.U.T.E.'s text channels, then you can manually add their role to the Category's permissions.

## Mass Move (`<massmove`)
Want to move a large group of people from one channel to another? Is dragging every person individually tiring? Well this command is here to save the day! Using this command, anyone with the `Move Members` permission can move users from one channel to the other with ease.

Do note the bot needs the `Move Members` permission set globally for this command to function correctly.

## Screenshare Easily On Any Server (`<link`, **enabled** by default)
Want to screenshare on your server? M.U.T.E. makes this easy by adding a link in the Voice-Text channel description which brings up the screenshare UI for your voice channel. This feature is enabled by default; to disable it, simply run `<link` in any channel.

## Screenshare Blocking (`<screenshare`, **disabled** by default)
People screensharing on your server when you don't want them to? Want to restrict screensharing to certain roles? Use `<screenshare` to enable screenshare blocking, which voicekicks anyone who tries to screenshare. The bot will **not** voicekick users who are above its own role. If you plan on blocking screensharing on your server then I recommend hiding the Voice Channel link from the text channel description via `<link`.

Do note the bot needs the `Move Members` and `Manage Channels` permission set globally for this feature to function correctly.

### Ignoring Channels
If, for whatever reason, you don't want M.U.T.E. to create a text channel for a certain voice channel, then remove M.U.T.E.'s `View Channel` permission for that specific channel.

## FAQs and Shit

### What does the bot use the permissions for?
 - **Manage Roles** - This is used for managing the permissions of the text channels. The bot does not create or edit roles in any way, permission related stuff just falls under this permission.
 - **Manage Channels** - For, well, creating and deleting the text channels. Duh.
 - **Read Messages** - The bot needs to read your commands somehow, and without this permission I can't actually edit the channels even if I have the `Manage Channels` permission. It's silly, I know.
 - **Send Messages** - So the bot can respond to commands and to announce people joining/leaving channels.
 - **Move Members** - Used for `<voicekick` and for screenshare blocking. Remove this if you don't plan on using these features.

If you don't want the bot to be able to do all of this globally, you can remove these permissions and add a channel specific override to the category for the bot with the `Manage Channel`, `Manage Permissions`, `Read Text Channels & See Voice Channels` and `Send Messages` permissions. Just make sure the bot has the `View Channel` permission for the voice channels you want it to see.

### The bot isn't creating a text channel
Ensure the bot's permissions for its category are correct. Also, make sure the bot has the `View Channel` permission, otherwise it will ignore the channel. Any channel marked as AFK in the server settings will also be ignored. If you're still having issues, then create an issue on this repository and I'll see how I can help you out.

### The bot isn't voicekicking people who are screensharing
Check to make sure the bot has the `Move Members` and `Manage Channels` permissions and make sure its role is above any roles that you'd like to restrict. Oh, and obviously make sure the feature is enabled by running `<screenshare`.

### I edited one of the voice text channels and the bot recreated it
Don't edit the text channels it makes, it destroys the channel and recreates it. The reason why is to avoid issues arising from people editing the permissions. No touchy. Capeesh?

### I'm a server admin and the unread message indicators annoy me
You can mute the entire category by right clicking it and selecing the mute option. If you still want to see new messages in there (if you're in a voice channel yourself) then you can uncollapse the category to see any new messages.

### Something's broken and I'm angry and upset and it's all your fault AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Create an issue on this repository describing the problem or holla@derpychap.co.uk.

### Why is it called M.U.T.E.? What's the point of this bot?
Well, the main group of people who'll benefit the most are "mutes" (people without microphones), hence the name. The anacronym is just to make it look cool. Having a text chat specifically for the voice channel that they're in (similar to how TeamSpeak, Mumble, etc. work) is incredibly useful for them as it allows them to easily participate in any voice discussions without using a single clusterfuck of a text channel made for *all* voice channels or just spamming the server's #general channel with half a conversation. Also, this setup allows you to share links, images, etc. to *just* the people in your voice channel. Everyone benefits! Neat, huh?!?

### The name sucks
no u

## Limitations
 - The bot will fill up the audit log with its actions. There's nothing I can do about this. The best workaround for this is to use the Audit Log's filtering options.
 - The chat history will be visible to new participants. Again, there's nothing I can do to change this.
 - Server admins will always be able to see the channels. Any conversations in there will not be private from them.
 - The text channels the bot creates aren't marked as NSFW, so, uh, don't post any NSFW stuff in them. I'm not responsible for anything that happens if you do. (And no, I won't make them NSFW)
 - Other bots might not like what this bot does (specifically bots that log actions and does actions based on the creation/deletion of channels, etc.). If that's the case, I recommend contacting that bot's creator for a category blacklist feature if the bot does not include one.

## Self-Hosting
### Prerequisites
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### `.env` File
Liara supports environment variables as config, so copy `example.env` to `bot.env` and fill in the details.

### Image
If you want to update the image, do `docker-compose pull`.  
Docker will pull a fresh image automatically on first run if you don't have one saved locally.

### Running the Bot
Start the service using `docker-compose up`  
This will start a Redis server and the Liara instance. It starts running in the foreground. `CTRL+C` to stop. You can also run it in detatched mode with `docker-compose up -d`.  
When you're done, `docker-compose rm` to remove the leftover containers.

On first boot your prefix will be randomised. To see what your prefix is, check the bot's logs with `docker-compose logs -f`. Use `[p]set prefix` to change it.

### Loading Cogs
In Discord, run the command `[p]load cogs.bot.manage`, and then run `[p]loadall` to load all the cogs in the `bot` folder.

## Maintainers
- DerpyChap (holla@derpychap.co.uk)
