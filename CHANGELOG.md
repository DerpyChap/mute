# Changelog
This is the changelog file. Any changes should be logged here as long as I remember to. I'm a professional!

## 16th May 2019
### Added
 - `<massmove` command for mass moving users from one channel to another
 - Things may or may not break when using this
 - Added icon for bots

### Changed
 - Voicekicking (used for `<voicekick` and screenshare blocking) has been reworked and now disconnects users directly instead of creating a channel, moving them and then deleting the channel
 - `<voicekick` has been made a hidden command as users with the Move Members permission can now Right Click > Disconnect users, making the command redundant

## 21st March 2019
### Added
 - Added mobile and owner indicators to join/leave messages
 - M.U.T.E. will no longer mention users if they've been mentioned before on a per server basis (Note that moving/changing the Voice Text category will reset this)
    - This change aims to make users aware of the text channel's existence and position while avoiding regulars being annoyed by the ping every time they join a voice channel

### Changed
 - Updated M.U.T.E.'s internals to support the latest version of the discord.py rewrite
 - Modified the changelog embed formatting to improve readability

### Fixed
 - The bot would not remove you properly from the voice-text channel if you switched to AFK
 - `<help` would break if a command had no help text associated with it
 - Added help text to `<voicekick` (oops)

## 18th February 2019
### Added
 - The bot is now able to automatically voicekick anyone who tries to screenshare (Use `<screenshare` to toggle, disabled by default)
 - `<voicekick` now supports an optional reason argument, which is logged in the Audit Log

### Changed
 - The output of `<link` and `<screenshare` vary based on if either option is enabled/disabled

## 11th February 2019

### Changed
 - Changed how the bot handles connection issues and unavailable guilds.

## 19th January 2019

### Added
 - `<voicekick` command will remove the target user from a voice channel. Bot requires the `Move Members` permission set globally.
 - Anyone with the `Move Members` permission can use this command, but cannot use it against someone who is equal to or higher than them in the role hirearchy.

### Fixed
 - More fixes to make sure channels do clean up correctly and stuff. Knowing my luck, it's probably still broken.

## 17th January 2019

### Fixed
 - Okay *NOW* I've fixed the channels not clearing properly issue. Probably. Maybe.

## 16th January 2019
### Added
 - Voice Text channels now have a 10 second inactivity cooldown before they're removed
 - The bot will now announce when it's received an update in active Voice Text channels
 - `<changes` command will list M.U.T.E.'s latest changelog
 - `<link` will toggle visibility of the Voice Channel URL in the description

### Fixed
 - Voice Text channels not clearing themselves properly if a user switches channels too fast

## 8th January 2019, 20:10PM GMT

### Added
 - The text channel description will now specify the channel name
 - The text channel description also has a link to the Voice Channel

### Changed
 - The text channel will now update its name when the voice channel is edited

### Fixed
 - The `invite` cog would fail to load on startup

## 8th January 2019

### Added
 - Added CHANGELOG.md and cog
 - Added contributing guide (`CONTRIBUTING.md`)
